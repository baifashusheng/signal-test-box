/*
@file: SignalEditor.h
@author: ZZH
@date: 2022-11-18
@info: 信号编辑器
*/
#pragma once
#include <QTextEdit>
#include <QListWidgetItem>

class SignalEditor: public QTextEdit
{
    Q_OBJECT
private:

protected:

public:
    SignalEditor(QWidget *parent = nullptr): QTextEdit(parent) {}
    SignalEditor(const QString &text, QWidget *parent = nullptr): QTextEdit(text, parent) {}
    ~SignalEditor() {}

    void addSelectedItem(QListWidgetItem* item);

    virtual void keyPressEvent(QKeyEvent* e) override;
};
